__author__ = 'justingibbons'
from Bio import SeqIO
from Bio.SeqUtils import GC
def sliding_window_generator(string,window_size,include_hangover=False):
    """Takes a string and yields chunks of the string of size window. If include_hangover is set to True any left over
    sequence that was too small to fit in the window will be included. Yields a tuple with the slice and the start and end
    index"""
    string_length=len(string)
    window_range=string_length-window_size+1 #This is the range of numbers that can be used as the start index for slicing

    if window_range<0:
        raise ValueError("Window size is larger than sequence!")



    for i in range(window_range):
        #print string[i:i+window_size]
        yield (string[i:i+window_size],i,i+window_size)

    if include_hangover and len(string[window_range:string_length])>0: #If you want the left overs and there are some yield them
        #print string[window_range:]
        yield (string[window_range:string_length],window_range,string_length)





def fasta_file_parser(fasta_file):
    """Generator function that returns a biopython records object from a fasta filexs"""
    for record in SeqIO.parse(fasta_file,"fasta"):
        yield record

def sliding_window_splice_fasta_record(fasta_record,window_size,include_hangover=False):
    """Takes a fasta record and yield new records from the slices. Modifies the ID to include the slice indexes like so
    OriginalID_startindex-endindex"""
    for slice in sliding_window_generator(fasta_record,window_size=window_size,include_hangover=include_hangover):
        fasta_record,start_index,end_index=slice
        fasta_record.id=fasta_record.id+"_"+str(start_index)+"-"+str(end_index)
        #print fasta_record
        yield fasta_record

def apply_sliding_window_splice_fasta_record_to_file(fasta_file,window_size,include_hangover=False):
    """Same as sliding_window_splice_fasta_record expect takes a fasta file"""
    for record in fasta_file_parser(fasta_file):
        for slice in sliding_window_splice_fasta_record(record,window_size=window_size,include_hangover=include_hangover):
            yield slice

def gc_content_reaches_threshold(sequence,min_gc):
    """Returns True if GC content of a sequence is greater than or equal to min_gc"""
    if GC(sequence)>=min_gc:
        return True
    else:
        return False

def yield_records_with_gc_content_that_reaches_threshold(record_iterable,min_gc):
    """Takes an iterable of sequence records and yields those sequences that have a GC content greater than or equal
    too min_gc content"""
    for record in record_iterable:
        if gc_content_reaches_threshold(record.seq,min_gc=min_gc):
            yield record
        else:
            continue

def slice_fasta_records_and_write_out_if_min_gc_content(in_fasta,out_fasta,window_size=500,include_hangover=True,min_gc=23,output_format="fasta"):
    """Takes a fasta file and for every file splits the sequence into windows of window_size and if the gc content of that
    sequence is greater than or equal to min_gc the sequence is written to out_fasta"""
    slices=apply_sliding_window_splice_fasta_record_to_file(fasta_file=in_fasta,window_size=window_size,include_hangover=include_hangover)
    pass_gc_filter=yield_records_with_gc_content_that_reaches_threshold(slices,min_gc=min_gc)
    SeqIO.write(pass_gc_filter,handle=out_fasta,format=output_format)




for record in fasta_file_parser("Test_Files/smaller_sample_fasta.fasta"):
    sliding_window_splice_fasta_record(record,window_size=5,include_hangover=True)
if __name__=="__main__":
    import unittest
    from compare_files_tools import get_file_lines
    class TestSequenceAnalysisTools(unittest.TestCase):
        def setUp(self):
            self.fasta_file="Test_Files/sample_fasta.fasta"
            self.output_slice_20="Test_Files/Output/result_filter_slices_by_gc_20.fasta"
            self.ref_output_slice_20="Test_Files/Ref_Files/ref_smaller_sample_fasta_slice_gc_20.fasta"
            self.smaller_fasta_file="Test_Files/smaller_sample_fasta.fasta"
            self.id_key="id"
            self.desc_key="description"
            self.sequence_key="sequence"

            self.record1=("PF3D7_0508900","AATGTAATAGAAATATATATATATATATATATATATATATATACATATGTAATAACAATATTATATATGTGAAGAAGAAATTATTTAATATATGACAGACTTTGAAATTTTTTGTTAATAATTAAAGCCAATTTTATATCATTTAATTTTTTAAAAAATATATAATATATTATATATATATATAATTATATTTATATGACCTTTTCATTTCATTTTATTCTTTTCTTTTGCCTTTCCCTT")

            self.record2=("PF3D7_0524200","CTTACAGATGAAAAAGGCAAATATAATCATGCATGAACATTTAGAAAATAATCTACAAAACCAAGATAAAAAGGAAGAAGATAAAACAAAGAATAATGACCAAAGGGCGGATGAAAATAA")
            self.record3=("PF3D7_1003000","TTGTCCTGGGTTTTTTTTTATATTTTCTTTCTTTTATTCTTACTTATTGTGTATTTATATATTTTTCATTCATTTATTCATCCTATATAATGGTTTCATTATATAAAGAATGAAATTGTATTATTATAGATTTTATTTATTAAATTAAGAAAAAATAATAAATATGTATCTTTTTGTATA")

        def test_fasta_file_parser(self):
            correct=[self.record1,self.record2,self.record3]
            result=[]
            for record in fasta_file_parser(self.fasta_file):
                result.append((record.id,record.seq))
            self.assertEqual(result,correct)


        def test_sliding_window_generator(self):
            string="0123456789"

            string_window3=[("012",0,3),("123",1,4),("234",2,5),("345",3,6),("456",4,7),("567",5,8),("678",6,9),("789",7,10),("89",8,10)]
            result_string_window3=[]
            for s in sliding_window_generator(string,window_size=3,include_hangover=True):
                result_string_window3.append(s)
            self.assertEqual(result_string_window3,string_window3)

        def test_sliding_window_splice_fasta_record(self):
            for record in fasta_file_parser(self.smaller_fasta_file):
                correct=[("PF3D7_0508900_0-5","AATGT"),("PF3D7_0508900_1-6","ATGTA"),("PF3D7_0508900_2-7","TGTAA"),("PF3D7_0508900_3-8","GTAAT"),("PF3D7_0508900_4-9","TAATA"),("PF3D7_0508900_5-10","AATAG"),("PF3D7_0508900_6-10","ATAG")]
                result=[]
                for slice in sliding_window_splice_fasta_record(record,5,include_hangover=True):
                    result.append((slice.id,slice.seq))

                self.assertEqual(result,correct)

        def test_apply_sliding_window_splice_fasta_record_to_file(self):
            correct=[("PF3D7_0508900_0-5","AATGT"),("PF3D7_0508900_1-6","ATGTA"),("PF3D7_0508900_2-7","TGTAA"),("PF3D7_0508900_3-8","GTAAT"),("PF3D7_0508900_4-9","TAATA"),("PF3D7_0508900_5-10","AATAG"),("PF3D7_0508900_6-10","ATAG")]
            result=[]
            for slice in apply_sliding_window_splice_fasta_record_to_file(self.smaller_fasta_file,5,include_hangover=True):
                result.append((slice.id,slice.seq))

            self.assertEqual(result,correct)


        def test_gc_content_reaches_threshold(self):
            seq="ACTGN"
            self.assertTrue(gc_content_reaches_threshold(seq,40))
            self.assertTrue(gc_content_reaches_threshold(seq,25))
            self.assertFalse(gc_content_reaches_threshold(seq,41))

        def test_slice_fasta_records_and_write_out_if_min_gc_content(self):
            slice_fasta_records_and_write_out_if_min_gc_content(self.smaller_fasta_file,self.output_slice_20,window_size=5,include_hangover=True,
                                                                min_gc=20)
            result,ref=get_file_lines(self.output_slice_20,self.ref_output_slice_20)
            self.assertEqual(result,ref)




    unittest.main()

