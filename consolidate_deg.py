__author__ = 'justingibbons'
import os
from get_set_from_file import get_set_from_column

def go_to_directories_and_execute_function(list_of_directories,function, **kwargs):
    """Goes through a list of directories and excutes the supplied function after each run of the function
        returns to the current directory"""

    current_dir=os.getcwd()
    for dir in list_of_directories:
        os.chdir(dir)
        yield function(**kwargs)
        os.chdir(current_dir)

def get_list_of_files_that_start_with(starts_with):
    """Returns a list of files in the current directory that starts with startswith. Makes sure is a regular file
    and not a directory"""
    files=os.listdir(".")

    return [file for file in files if os.path.isfile(file) and file.startswith(starts_with)]

def get_column_set_from_different_dirs(list_of_directories,starts_with,column_index=0,header=True,delimiter="\t"):
    """Goes to each of the directories in list_of_directories and creates a set from the data in column_index
    for each file whose name starts with starts_with"""
    master_set=set()

    for file_list in go_to_directories_and_execute_function(list_of_directories,get_list_of_files_that_start_with,starts_with=starts_with):
        for file in file_list:
            col_set=get_set_from_column(infile=file,header=header,delimiter=delimiter,column_index=column_index)
            master_set.update(col_set)
    return master_set

def get_column_set_from_different_dirs_with_multiple_starts_with(list_of_directories,starts_with_list,column_index=0,header=True,delimiter="\t"):
    """Similar to get_set_from_different_dirs but accepts multple starts_with strings as a list"""
    master_set=set()
    for starts_with in starts_with_list:
        file_sets=get_column_set_from_different_dirs(list_of_directories=list_of_directories,
                                                     starts_with=starts_with,
                                                     column_index=column_index,
                                                     header=True,
                                                     delimiter=delimiter)
        master_set.update(file_sets)

    return master_set



if __name__=="__main__":
    import unittest

    class TestConsolidateDEGExpression(unittest.TestCase):
        def setUp(self):
            self.list_of_dir=["Test_Files/Dir1","Test_Files/Dir2"]
            self.starts_with1="downreg"
            self.startswith2="upreg"
            self.starts_with_list=[self.starts_with1,self.startswith2]

        def test_go_to_directories_and_execute_function(self):
            correct_answer=[['downreg_timepoint1.txt'],['downreg_timepoint2.txt']]
            response=[]


            for result in go_to_directories_and_execute_function(self.list_of_dir,get_list_of_files_that_start_with,starts_with=self.starts_with1):
                response.append(result)
            self.assertEqual(response,correct_answer)

        def test_get_column_set_from_different_dirs(self):
            correct_answer={"PF3D7_0300200","PF3D7_0401500","PF3D7_0402000","PF3D7_0412700","PF3D7_0424900",
                            "PF3D7_0425900","PF3D7_0533000"}
            self.assertEqual(get_column_set_from_different_dirs(self.list_of_dir,self.starts_with1,column_index=0),correct_answer)

        def test_get_column_set_from_different_dirs_with_multiple_starts_with(self):
            correct_answer={"PF3D7_0300200","PF3D7_0401500","PF3D7_0402000","PF3D7_0412700","PF3D7_0424900",
                            "PF3D7_0425900","PF3D7_0533000","PF3D7_0207700","PF3D7_0405900"}
            self.assertEqual(get_column_set_from_different_dirs_with_multiple_starts_with(self.list_of_dir,self.starts_with_list,
                                                                                          column_index=0),correct_answer)


    unittest.main()